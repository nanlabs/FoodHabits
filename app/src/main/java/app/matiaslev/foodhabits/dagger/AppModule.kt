package app.matiaslev.foodhabits.dagger

import android.arch.persistence.room.Room
import android.content.Context
import app.matiaslev.foodhabits.App
import app.matiaslev.foodhabits.dagger.components.FoodSubcomponent
import app.matiaslev.foodhabits.database.AppDatabase
import dagger.Module
import dagger.Provides


/**
 * Created by matiaslev on 6/17/17.
 */

/**
 * Application module refers to sub components and provides application level dependencies.
 */
@Module(subcomponents = arrayOf(FoodSubcomponent::class))
class AppModule {
    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Provides
    internal fun provideDatabase(context: Context) : AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "food")
                .allowMainThreadQueries()
                .build()
    }
}