package app.matiaslev.foodhabits.dagger

import app.matiaslev.foodhabits.ui.food.FoodActivity
import app.matiaslev.foodhabits.ui.food.NewFoodDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * Created by matiaslev on 6/18/17.
 */

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    internal abstract fun bindFoodActivity(): FoodActivity

    @ContributesAndroidInjector
    internal abstract fun bindNewFoodFragment(): NewFoodDialogFragment

    // Add more bindings here for other sub components
}