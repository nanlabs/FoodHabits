package app.matiaslev.foodhabits.dagger.components

import app.matiaslev.foodhabits.ui.food.NewFoodDialogFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by matiaslev on 6/27/17.
 */

@Subcomponent
interface NewFoodSubcomponent : AndroidInjector<NewFoodDialogFragment> {
    @Subcomponent.Builder
    abstract class builder : AndroidInjector.Builder<NewFoodDialogFragment>()
}