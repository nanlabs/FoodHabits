package app.matiaslev.foodhabits.database

import android.arch.persistence.room.TypeConverter
import org.joda.time.LocalDate
import org.joda.time.LocalTime

/**
 * Created by matiaslev on 7/1/17.
 */

class Converters {
    @TypeConverter
    fun stringToLocalDate(value: String): LocalDate {
        return value.let {
            LocalDate.parse(value)
        }
    }

    @TypeConverter
    fun localDateToString(date: LocalDate): String {
        return date.let {
            date.toString()
        }
    }

    @TypeConverter
    fun stringToLocalTime(value: String): LocalTime {
        return value.let {
            LocalTime.parse(value)
        }
    }

    @TypeConverter
    fun localTimeToString(date: LocalTime): String {
        return date.let {
            date.toString()
        }
    }
}