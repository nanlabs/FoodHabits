package app.matiaslev.foodhabits.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import app.matiaslev.foodhabits.models.food.Food
import app.matiaslev.foodhabits.models.food.FoodDao

/**
 * Created by matiaslev on 6/14/17.
 */

@Database(entities = arrayOf(Food::class), version = 5, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun foodDao(): FoodDao
}