package app.matiaslev.foodhabits.models.food

import android.arch.lifecycle.LiveData
import app.matiaslev.foodhabits.database.AppDatabase
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import javax.inject.Inject

/**
 * Created by matiaslev on 6/22/17.
 */

class FoodRepository @Inject constructor(val database: AppDatabase) {

    fun getAllFoodsFromOneDay(mediumDate: LocalDate): LiveData<List<Food>> {
        return database.foodDao().getAllFoodsFromOneDay(mediumDate)
    }

    fun getFoodsToShare(mediumDate: LocalDate): List<Food> {
        return database.foodDao().getFoodsToShare(mediumDate)
    }

    fun removeFoods() {
        database.foodDao().deleteAllFoods()
    }

    fun addNewFood(hour: LocalTime, name: String, mediumDate: LocalDate) {
        database.foodDao().insertOneFood(Food(0, name, hour, mediumDate))
    }

    fun deleteOneFood(food: Food) {
        database.foodDao().deleteOneFood(food)
    }

}