package app.matiaslev.foodhabits.models.food

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import org.joda.time.LocalDate

/**
 * Created by matiaslev on 6/14/17.
 */

@Dao
interface FoodDao {

    @Query("select * from foods")
    fun getAllFoods(): LiveData<List<Food>>

    @Query("select * from foods where date = :arg0 ORDER BY hour")
    fun getAllFoodsFromOneDay(date: LocalDate): LiveData<List<Food>>

    @Query("select * from foods where date = :arg0")
    fun getFoodsToShare(date: LocalDate): List<Food>

    @Insert
    fun insertOneFood(food: Food)

    @Query("delete FROM foods")
    fun deleteAllFoods()

    @Delete
    fun deleteOneFood(food: Food)
}