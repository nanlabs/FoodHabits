package app.matiaslev.foodhabits.extensions

import java.text.DateFormat
import java.util.*

/**
 * Created by matiaslev on 7/2/17.
 */

fun Date.toMediumDate() : String {
    // Jan 3, 2000
    return getMediumDateFormat().format(this)
}

fun getMediumDateFormat(): DateFormat {
    return DateFormat.getDateInstance(DateFormat.MEDIUM)
}