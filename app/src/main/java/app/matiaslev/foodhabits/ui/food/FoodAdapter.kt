package app.matiaslev.foodhabits.ui.food

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import app.matiaslev.foodhabits.R
import app.matiaslev.foodhabits.extensions.inflate
import app.matiaslev.foodhabits.models.food.Food
import kotlinx.android.synthetic.main.food_item.view.*
import javax.inject.Inject

/**
 * Created by matiaslev on 6/11/17.
 */

class FoodAdapter @Inject constructor(val foodActivity: FoodActivity) : RecyclerView.Adapter<ViewHolder>() {
    var foods: List<Food> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.food_item))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (position == foods.size) {
            viewHolder.bind(Food())
        } else {
            viewHolder.bind(foods[position])
            viewHolder.itemView.imageButtonDelete.setOnClickListener({
                foodActivity.deleteItem(foods[position])
            })
        }
    }

    override fun getItemCount(): Int {
        return foods.size+1
    }

    interface DeleteItem {
        fun deleteItem(food: Food)
    }

}





