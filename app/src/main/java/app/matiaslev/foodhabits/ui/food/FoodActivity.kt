package app.matiaslev.foodhabits.ui.food

import android.arch.lifecycle.LifecycleActivity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import app.matiaslev.foodhabits.R
import app.matiaslev.foodhabits.models.food.Food
import app.matiaslev.foodhabits.models.food.FoodViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import org.joda.time.LocalDate
import java.util.*
import javax.inject.Inject


class FoodActivity : LifecycleActivity(),
        NewFoodDialogFragment.NewFoodInterface,
        FoodAdapter.DeleteItem {

    @Inject lateinit var foodViewModel: FoodViewModel
    lateinit var foodAdapter: FoodAdapter

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        AndroidInjection.inject(this)
        foodAdapter = FoodAdapter(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecycler()
        addCalendarListener()
        addFoodListener()
        addShareListener()
    }

    override fun onResume() {
        super.onResume()
        observeFoods()
    }

    fun addCalendarListener() {
        calendarView.setOnDateChangedListener({ widget, date, selected ->
            notifyChange()
        })
    }

    override fun notifyChange() {
        observeFoods()
    }

    fun observeFoods() {
        Log.d("date: ", getCalendarDate().toString())
        foodViewModel.getAllFoodsFromOneDay(getCalendarDate())
                .observe(this, Observer<List<Food>> {
                    foods -> foods?.let {
                        foodAdapter.foods = it
                        foodAdapter.notifyDataSetChanged()
                        refreshVisivility(foods)
                    }
                })
    }

    fun getCalendarDate(): LocalDate {
        if (calendarView.selectedDate != null) {
            val calendarDate = calendarView.selectedDate
            return LocalDate(calendarDate.year, calendarDate.month+1, calendarDate.day)
        } else {
            calendarView.setSelectedDate(Date())
            val calendarDate = calendarView.selectedDate
            return LocalDate(calendarDate.year, calendarDate.month+1, calendarDate.day)
        }
    }

    fun refreshVisivility(foods: List<Food>) {
        textViewQuestion.visibility = getQuestionVisivility(foods)
        imageViewFood.visibility  = getQuestionVisivility(foods)
    }

    fun getQuestionVisivility(foods: List<Food>): Int {
        return if (foods.isNotEmpty()) View.GONE else View.VISIBLE
    }

    fun initRecycler() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = foodAdapter
    }

    fun addFoodListener() {
        floatingActionButton.setOnClickListener {
            val newFoodDialogFragment = NewFoodDialogFragment()
            newFoodDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 4)
            newFoodDialogFragment.arguments = setCalendarDate()
            newFoodDialogFragment.show(fragmentManager, getString(R.string.food))
        }
    }

    fun addShareListener() {
        buttonShare.setOnClickListener({
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            setFoodsToShare(sendIntent)

            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        })
    }

    fun setFoodsToShare(sendIntent: Intent) {
        val foods: List<Food> = foodViewModel.getFoodsToShare(getCalendarDate())
        sendIntent.putExtra(Intent.EXTRA_TEXT, foods.toString())
    }

    fun setCalendarDate(): Bundle {
        val arguments = Bundle()
        arguments.putSerializable("mediumDate",  getCalendarDate())
        return arguments
    }

    override fun deleteItem(food: Food) {
        foodViewModel.deleteFood(food)
    }

}
