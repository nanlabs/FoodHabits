package app.matiaslev.foodhabits.ui.food

import android.support.v7.widget.RecyclerView
import android.view.View
import app.matiaslev.foodhabits.models.food.Food
import kotlinx.android.synthetic.main.food_item.view.*

/**
 * Created by matiaslev on 6/12/17.
 */

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(food: Food) = with(itemView) {
        textViewName.text = food.name
        textViewHour.text = food.hour.toString()
        if (food.name.isEmpty()) {
            imageButtonDelete.visibility = View.GONE
            textViewHour.visibility = View.GONE
        } else {
            imageButtonDelete.visibility = View.VISIBLE
            textViewHour.visibility = View.VISIBLE
        }
    }
}