package app.matiaslev.foodhabits

/**
 * Created by nanlabs on 23/11/17.
 */

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import app.matiaslev.foodhabits.ui.food.FoodActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class EspressoTest {

    @Rule @JvmField
    val mActivityRule = ActivityTestRule<FoodActivity>(
            FoodActivity::class.java)

    // this just make sense with an empty database
    @Test
    fun addFood() {
        onView(withId(R.id.floatingActionButton))
                .perform(click())
        onView(withId(R.id.editTextName))
                .perform(typeText("milanesas c/pure"), closeSoftKeyboard())
        onView(withId(R.id.buttonAddNewFood))
                .perform(click())
        onView(withId(R.id.recyclerView))
                .check(matches(hasDescendant(withText("milanesas c/pure"))))
    }
}



