package app.matiaslev.foodhabits

import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec

/**
 * Created by nanlabs on 12/11/17.
 */

class ExampleTest : StringSpec() {
    init {
        "length should return size of string" {
            "hello".length shouldBe 5
        }
    }
}